/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interpreter.analex.interfaces;

import interpreter.events.TokenEvent;

/**
 *
 * @author ronal
 */
public interface ITokenEventListener {
    
    void user(TokenEvent event);   
    void client(TokenEvent event);     
    void dpto(TokenEvent event);            
    void social(TokenEvent event);      
    void schedule(TokenEvent event);    
    void notify(TokenEvent event);
    void apartment(TokenEvent event); 
    void visit(TokenEvent event); 
    void support(TokenEvent event);
    void reserve(TokenEvent event);
    void veterinario(TokenEvent event);//como ejercicio
    void mascota(TokenEvent event);//como ejercicio
    
    void error(TokenEvent event);
}
